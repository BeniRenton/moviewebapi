﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieWebApi.Models;

namespace MovieWebApi.Data
{
    public class MovieWebApiContext : DbContext
    {
        public MovieWebApiContext (DbContextOptions<MovieWebApiContext> options)
            : base(options)
        {
        }

        public DbSet<MovieWebApi.Models.Movie> Movie { get; set; }
    }
}
